import logging

from google.appengine.api import memcache
from server.models import Link
import shortner


def getLinkByUrl(url):
    """
    Get a link entity relative to an orignal url.
    Check the cache and then check the datastore
    """
    link = Link.query(Link.shortUrl == shortUrl).get()
    return link


def getLinkByShortUrl(shortUrl):
    """
    Return link entity relatie to the short url passed in parameter.
    """
    link = link.query(Link.shortUrl == shortUrl).get()
    return link


def saveLink(url):
    """
    Save the url in parameter in an entity Link.
    """
    link = Link(url=url)
    link.put()
    link.shortUrl = shortner.dehydrate(link.key.id())
    link.put()


    return link
